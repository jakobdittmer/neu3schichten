package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {
	private File file = new File("src/data/", "cooleausgabe.txt");
	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		try {
			FileWriter fw = new FileWriter(file, true);
			PrintWriter bw = new PrintWriter(fw);
			bw.println(code);
		    bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(promocodes.toString());
			return this.promocodes.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		try {
		    FileReader fr = new FileReader(file);
		    BufferedReader br = new BufferedReader(fr);
		    
		    while (true){
		    	String p = br.readLine();
		    	if (p == null) break;
		    	promocodes.add(p);
		      }   
		    br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		return this.promocodes.contains(code);
	}

}
