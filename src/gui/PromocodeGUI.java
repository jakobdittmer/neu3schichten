package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;


import logic.PromoCodeLogic;

import javax.swing.JTextPane;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;
	private JTextPane txtpnHierKnnteIhr;
	private JLabel lblIhrPromocode;

	/**
	 * Create the frame.
	 * change for push
	 */
	public PromocodeGUI(PromoCodeLogic logic) {
		setTitle("BND-Virus");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 587);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		lblIhrPromocode = new JLabel("ihr promocode");
		contentPane.add(lblIhrPromocode, BorderLayout.WEST);
		this.setVisible(true);
		
		lblPromocode = new JLabel("cool generator");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		contentPane.add(lblPromocode, BorderLayout.NORTH);
		
		JButton btnNewPromoCode = new JButton("GIB MIR CODE!");
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblIhrPromocode.setText(code);
			}
		});
		contentPane.add(btnNewPromoCode, BorderLayout.SOUTH);
		
		txtpnHierKnnteIhr = new JTextPane();
		txtpnHierKnnteIhr.setText("hier k\u00F6nnte ihr promocode stehen");
		contentPane.add(txtpnHierKnnteIhr, BorderLayout.CENTER);
		
		
	}

}
